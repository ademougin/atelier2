MyApp.controller ( 'PhotosAddController' , [ '$scope' , 'fileUpload' , 'Serie', '$routeParams', '$rootScope', function ( $scope , fileUpload, Serie, $routeParams, $rootScope ) {

    var s = Serie.get ( {
        id : $routeParams[ 'id' ]
    });

    $scope.serie = s;

    $scope.uploadPhoto = function () {
        var img = $scope.myFile;

        var uploadUrl = 'http://10.10.173.44:8080/photolocate/api/img/upload';
        fileUpload.uploadFileToUrl ( img , uploadUrl );

    }


} ] );
