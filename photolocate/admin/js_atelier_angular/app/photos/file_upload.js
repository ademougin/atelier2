MyApp.service ( 'fileUpload' , [ '$http' , '$location', '$routeParams','$rootScope', 'Serie', function ( $http, $location , $routeParams, $rootScope, Serie) {

    //on récupère la série
    var s = Serie.get ( {
        id : $routeParams[ 'id' ]
    });

    this.uploadFileToUrl = function ( img , uploadUrl ) {
        var fd = new FormData ();
        fd.append ( 'file' , img );
        $http.post ( uploadUrl , fd , {
                transformRequest : angular.identity ,
                headers : { 'Content-Type' : undefined }
            } )
            .success ( function (response) {
                $rootScope.urlIMG = response;
                $rootScope.imgUploaded = true;
                $location.path ( "/series/"+s.id+"/maps" );

            } )
            .error ( function () {
            } );
    }
} ] );