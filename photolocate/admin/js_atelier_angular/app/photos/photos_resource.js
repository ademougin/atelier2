MyApp.factory('Photo', ['$resource', function ($resource) {

    var urlApi = "/photolocate/api";

    //URL d'une série
    return $resource(window.ws + urlApi + '/photos', {
        //Paramètre nécessaire lors d'une requête
    }, {
        //Methode de mise à jour
        update: {
            methode: 'PUT'
        }
    }, {
        //Methode de suppression
        delete: {
            methode: 'DELETE'
        }
    });
}]);