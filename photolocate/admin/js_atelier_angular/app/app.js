// adresse de l'api
window.ws = "http://10.10.173.44:8080";

var MyApp = angular.module ( 'admin' , [ 'ngSanitize' , 'ngRoute' , 'ngResource' , 'leaflet-directive' ] );

MyApp.config ( [ '$routeProvider' , function ( $routeProvider) {

    var cheminAdmin = "js_atelier_angular/";

    //Controller de l'accueil
    $routeProvider.when ( '/' , {
        templateUrl : cheminAdmin + "views/accueil.html" ,
        controller: 'AccueilController'
    } );

    $routeProvider.when ( '/series' , {
        templateUrl : cheminAdmin + "views/series/index.html" ,
        controller : 'SeriesIndexController'
    } );

    //Controller de l'ajout d'une photo
    $routeProvider.when ( '/series/:id' , {
        templateUrl : cheminAdmin + "views/photos/add.html" ,
        controller : 'PhotosAddController'
    } );

    //Controller de l'ajout d'une photo
    $routeProvider.when ( '/series/:id/maps' , {
        templateUrl : cheminAdmin + "views/maps/index.html" ,
        controller : 'MapsController'
    } );

    //Controller de la création d'une série
    $routeProvider.when ( '/new' , {
        templateUrl : cheminAdmin + "views/series/new.html" ,
        controller : 'SeriesNewController'
    } );

} ] );

MyApp.controller ( 'AccueilController' , [ '$scope' ,'$rootScope' , '$timeout', function ( $scope, $rootScope, $timeout ) {
    
}]);