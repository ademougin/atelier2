MyApp.controller ( 'MapsController' , [ '$scope' , 'seriesList' , '$http' , '$routeParams' , 'Serie' , 'Photo' , '$location' , '$rootScope' , '$timeout', function ( $scope , seriesList , $http , $routeParams , Serie , Photo , $location , $rootScope, $timeout ) {


//on initialise la carte vide
    angular.extend ( $scope , {
        ville : {
            lat : 50 ,
            lng : 20 ,
            zoom : 1
        } ,
        markers : {} ,
        events : {}
    } );

//on recupere les coordonnées par défaut de la serie APRES le callback
    var s = Serie.get ( {
        id : $routeParams[ 'id' ]
    } , function ( s ) {
        angular.extend ( $scope , {
            ville : {
                lat : parseFloat ( s.lat ) ,
                lng : parseFloat ( s.lon ) ,
                zoom : s.zoom
            } ,
            markers : {}
            , events : {
                map : {
                    enable : [ 'click' ] ,
                    logic : 'emit'
                }
            }
        } );

        $scope.latMap = "Non définie";
        $scope.lngMap = "Non définie";

        $scope.$on ( 'leafletDirectiveMap.click' , function ( event , args ) {
            $scope.latMap = args.leafletEvent.latlng.lat;
            $scope.lngMap = args.leafletEvent.latlng.lng;
            $scope.markers.select = {
                lat : $scope.latMap ,
                lng : $scope.lngMap
            }
        } );

    } , function ( echec ) {
    } );

    $timeout(function() {
        $rootScope.imgUploaded = undefined;
    }, 3000);

//on permet d'effectuer de srecherches pour trouver plus facilement un batiment
    $scope.searchVille = function () {
        var url = "https://maps.googleapis.com/maps/api/geocode/json?address=" + $scope.rech + "&key=AIzaSyAOFpkPeERx26L8whBBJ4fOSxX7X_xjxM8";
        $http.get ( url ).success ( function ( res ) {
            $scope.ville = {
                lat : res.results[ 0 ].geometry.location.lat ,
                lng : res.results[ 0 ].geometry.location.lng ,
                zoom : 19
            };
            $scope.rech = res.rech;
        } );
    };

   var imgURL = $rootScope.urlIMG;

//on ajoute une description
    $scope.addDescr = function () {
        var p = new Photo ( {
            description : $scope.descr ,
            lat : $scope.latMap + '' ,
            lon : $scope.lngMap + '' ,
            serie : s ,
            url : imgURL
        } );
        $rootScope.imgAdded = true;

        p.$save ( {} , function ( r ) {
            $location.path ( "/series" );
        } )
    }

} ] );