MyApp.controller ( 'SeriesNewController' , [ '$scope' , 'Serie' , '$location', function ( $scope , Serie, $location ) {


    $scope.newSerie = function () {
        var s = new Serie ( {
            "distance" : $scope.serieN.distance ,
            "lat" : $scope.serieN.lat +'' ,
            "lon" : $scope.serieN.lon +'' ,
            "ville" : $scope.serieN.ville ,
            "zoom" : $scope.serieN.zoom
        } )

        s.$save ( {} , function ( r ) {
            console.log ( "Série créée" );
            $location.path ( "/" );
        } );
    }
} ] );
