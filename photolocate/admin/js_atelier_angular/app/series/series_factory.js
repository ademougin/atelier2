MyApp.factory('seriesList', ['Serie', function ( Serie ) {

    // On initialise une liste vide
    var seriesList = {
        series : []
    };
    //Requête pour récupérer les series
    seriesList.series = Serie.query();


    // On retourne la liste de séries
    return seriesList;
}]);