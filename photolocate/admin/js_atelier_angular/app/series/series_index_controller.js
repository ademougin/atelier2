MyApp.controller('SeriesIndexController', ['$scope', '$resource', 'seriesList', 'Serie','$routeParams', '$rootScope', '$timeout', function($scope, $resource, seriesList, Serie, $routeParams, $rootScope, $timeout) {
    // On récupère la serie cliquée
    $scope.serie = Serie.query({
        id: $routeParams['id']
    });

	seriesList.series.$promise.then(function() {
		$scope.seriesList = seriesList.series;
	});

    $timeout(function() {
        $rootScope.imgAdded = undefined;
    }, 3000);

}]);
