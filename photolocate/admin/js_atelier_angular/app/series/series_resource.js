MyApp.factory('Serie', ['$resource', function($resource) {

    var urlApi = "/photolocate/api";

    //URL d'une série
    return $resource(window.ws + urlApi + '/series/:id', {
        //Paramètre nécessaire lors d'une requête
        id: '@id'
    }, {
        //Methode de mise à jour
        update: {
            methode: 'PUT'
        }
    }, {
        //Methode de suppression
        delete: {
            methode: 'DELETE'
        }
    });
}]);
