angular.module('photolocate').factory('Photos', ['$resource', function ($resource) {

    var urlApi = "/photolocate/api/photos";

    //URL d'une série
    return $resource(window.ws + urlApi);
}]);
