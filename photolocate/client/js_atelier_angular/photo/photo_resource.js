angular.module('photolocate').factory('Photo', ['$resource', function ($resource) {

    var urlApi = "/photolocate/api/series/:idPhoto/photos";

    //URL d'une série
    return $resource(window.ws + urlApi ,{idPhoto: '@idPhoto'});
}]);
