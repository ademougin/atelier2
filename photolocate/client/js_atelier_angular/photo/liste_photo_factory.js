angular.module("photolocate").factory("listePhotos", ['Photos', function(Photos){
  var listePhotos = {
    photos : []
  };

  listePhotos = Photos.query();
  return listePhotos;
}]);
