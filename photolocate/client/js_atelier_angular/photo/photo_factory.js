angular.module("photolocate").factory("photoJeu", ['Photo','$routeParams', function(Photo,$routeParams){
  var photoJeu = {
    description : "",
    idPhoto : "",
    idSerie : [],
    lat : "",
    lon : "",
    url : ""
  };

  photoJeu = Photo.get({idSerie:$routeParams['idSerie']});
  return photoJeu;
}]);
