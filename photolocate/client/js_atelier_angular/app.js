window.ws = "http://10.10.173.44:8080"; // Lien de l'api à laquelle on fera appel
window.token = ""; // Appel à l'api pour obtenir le token
window.api = "/photolocate/api"; // Chemin vers l'api du projet

//Variable
var _views = "js_atelier_angular/views/";


app = angular.module ( "photolocate" , [ "ngSanitize" , "ngRoute" , "ngResource" , "leaflet-directive" ] );

app.config ( [ '$routeProvider' , '$httpProvider' , function ( $routeProvider , $httpProvider ) {

    //$httpProvider.defaults.headers.common.Authorization = 'Token token='+window.token;

    $routeProvider.when ( '/' , {
        templateUrl : _views + "index.html" ,
        controller : 'initController'
    } );
    //On affiche toutes les séries
    $routeProvider.when ( '/series' , {
        templateUrl : _views + "liste_serie.html" ,
        controller : 'listeNewController'
    } );
    //On à choisis la série donc on l'ouvre
    $routeProvider.when ( '/parties/:id' , {
        templateUrl : _views + "serie.html"
    } );

    $routeProvider.when ( '/series/:id/newgame' , {
        templateUrl : _views + "new_game.html" ,
        controller : 'newGameController' ,
        resolve : {
            token : function ( Token ) {
                return Token.get ( {} ,
                    function ( t ) {
                        return t.token;
                    } );
            }
        }

    } );


    // On affiche les meilleurs scores avec le sien
    $routeProvider.when ( '/parties/:id/scores' , {
        templateUrl : _views + "score.html" ,
        controller : "scorePlayerController"
    } );

    // On affiche les meilleurs scores
    $routeProvider.when ( '/scores' , {
        templateUrl : _views + "score.html" ,
        controller : "scoreController"
    } );
} ] );

app.controller ( 'initController' , [ '$location' , '$scope' , 'Partie' , '$rootScope' , function ( $location , $scope , Partie , $rootScope ) {

} ] );
