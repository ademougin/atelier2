angular.module ( "photolocate" ).factory ( "serieJeu" , [ 'Serie' , '$routeParams' , function ( Serie , $routeParams ) {
    var serieJeu = function () {
        return Serie.get ( { idSerie : $routeParams[ 'idSerie' ] } )
    };
    return serieJeu;
} ] );
