angular.module ( "photolocate" ).controller ( "MapController" , [ '$location' , '$scope' , '$routeParams' , 'Partie' , '$rootScope' , function ( $location , $scope , $routeParams , Partie , $rootScope ) {

    var partie = Partie.get ( { id : $routeParams[ 'id' ] } );
    var serieJeu;
    partie.$promise.then ( function () {
        serieJeu = partie.serie;

        $scope.markers = new Array ();
        $scope.coo = serieJeu;
        angular.extend ( $scope , {
            ville : {
                lat : parseFloat ( serieJeu.lat ) ,
                lng : parseFloat ( serieJeu.lon ) ,
                zoom : parseFloat ( serieJeu.zoom )
            } ,

            defaults : {
                zoomControl : false ,
                scrollWheelZoom : false ,
                keyboard : false ,
                dragging : false ,
                boxZoom : false ,
                doubleClickZoom : false ,
                touchZoom : false
            } ,


            events : {
                markers : {
                    enable : [ 'click' ] ,
                    logic : 'emit'
                }
            }
        } );

        var icons = {
            red : {
                type : 'div' ,
                iconSize : [ 10 , 10 ] ,
                className : 'red' ,
                iconAnchor : [ 5 , 5 ]
            } ,

            green : {
                type : 'div' ,
                iconSize : [ 12 , 12 ] ,
                className : 'icongreen' ,
                iconAnchor : [ 5 , 5 ]
            }
        }


        //Supprime le dernier marker et en ajoute un nouveau à la place
        $scope.$on ( 'leafletDirectiveMap.click' , function ( event , args ) {
            if ( $rootScope.cliquable === true ) {
                $rootScope.cliquable = false;
                $scope.markers.push ( {
                    lat : args.leafletEvent.latlng.lat ,
                    lng : args.leafletEvent.latlng.lng ,
                    icon : icons.red
                } );
                $rootScope.mapMarkers.push ( args.leafletEvent.latlng );
            }
        } );


        function addGreenCircle () {
            if ( angular.isDefined ( $rootScope.answers ) ) {
                if ( $rootScope.answers != [] || $rootScope.answers.length !== 0 ) {
                    $scope.markers.push ( {
                        lat : parseFloat ( $rootScope.answers[ 0 ].lat ) ,
                        lng : parseFloat ( $rootScope.answers[ 0 ].lon ) ,
                        icon : icons.green
                    } );
                }
            }
        }

        $scope.$watch ( function () {
            return $rootScope.answers;
        } , addGreenCircle , true );


    } );
} ] );
