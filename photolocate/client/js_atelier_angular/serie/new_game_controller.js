angular.module ( "photolocate" ).controller ( "newGameController" , [ '$location' , '$scope' , '$routeParams' , 'Serie' , 'serieJeu' , 'token' , 'Partie' , '$http' , function ( $location , $scope , $routeParams , Serie , serieJeu , token , Partie , $http ) {

    //FAIRE GET SERIE
    var s = Serie.get ( {
        id : $routeParams[ 'id' ]
    } );


    $scope.startGame = function () {
        var pseudo = $scope.pseudo;
        console.log ( s );

        var p = new Partie ( {
            joueur : pseudo ,
            serie : s
        } );

        p.$save ( {} , function ( r ) {
            console.log ( r );
            $location.path ( "/parties/" + r.id );
        } );

    }

}
] )
;
