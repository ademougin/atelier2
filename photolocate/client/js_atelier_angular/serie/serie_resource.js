angular.module ( 'photolocate' ).factory ( 'Serie' , [ '$resource' , function ( $resource ) {
    var urlApi = window.api + "/series/:id";

    return $resource ( window.ws + urlApi , { id : '@id' } );
} ] );
