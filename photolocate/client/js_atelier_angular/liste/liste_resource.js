// angular.module('photolocate').factory('Liste', ['$resource', function($resource){
//   var url = window.ws+'/PhotoLocateServer/api/'
//   return $resource(url+'series/', {id: '@id'}, {
//     update: {method : 'PUT'}
//   })
// }]);


angular.module('photolocate').factory('Liste', ['$resource', function ($resource) {

    //URL d'une série
    return $resource(window.ws + window.api + '/series/:id', {
        //Paramètre nécessaire lors d'une requête
        id : '@id'
    }, {
        //Methode de mise à jour
        update : {
            methode : 'PUT'
        }
    },{
        //Methode de suppression
        delete : {
            methode : 'DELETE'
    }
    });
}]);



//10.10.173.44:8080/PhotoLocateServer/api/series


// /series
//
//
//
// "series" : {
// 	"id" : ,
// 	"ville" : ,
// 	"lat" : ,
// 	"long" : ,
// 	"zoom" : ,
// 	"distance" : ,
// 	"photos" : {...}
// 	}
