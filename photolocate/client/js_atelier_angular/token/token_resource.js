angular.module('photolocate').factory('Token', ['$resource', function ($resource) {
    var urlApi = window.api + "/token";

    //URL d'un token
    return $resource(window.ws + urlApi);
}]);
