angular.module("photolocate").controller("scorePlayerController", ['$location', '$scope','$routeParams','Partie','scoreJeu','$rootScope', function($location, $scope,$routeParams,Partie,scoreJeu, $rootScope){
    $scope.score = scoreJeu;
    var all_scores = Partie.query ( { path : 'scores' } );
    all_scores.$promise.then(function() {

        $scope.bestScore = all_scores;
        if (typeof $routeParams['id'] !== 'undefined'){
            var own_score = Partie.get({id: $routeParams['id']});
                own_score.$promise.then(function() {
                    $scope.ownScore = own_score;
                });
        }
    });
}]);






angular.module('photolocate' ).controller('scoreController', ['Partie','$scope',function (Partie, $scope) {
        var all_bestscores = Partie.query ( { path : 'scores' } );

        all_bestscores.$promise.then ( function () {
            $scope.bestScore = all_bestscores;
        } );
}]);
