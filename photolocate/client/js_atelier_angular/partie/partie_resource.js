angular.module('photolocate').factory('Partie', ['$resource', function($resource) {    

	return $resource(window.ws + window.api + "/parties/:path:id",
    {
		//Paramètre nécessaire lors d'une requête
		path: "@path",
		id: "@id",
	});
}]);
