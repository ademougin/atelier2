//On gère le comportement du jeu ici !
angular.module("photolocate").controller("partieController", ['$http', '$location', '$scope', '$interval', '$routeParams', '$rootScope', '$timeout', 'scoreJeu', 'Photo', 'Partie', function($http, $location, $scope, $interval, $routeParams, $rootScope, $timeout, scoreJeu, Photo, Partie) {

  $scope.tableauDistPhoto = new Array();
  $scope.nbImg = 0;
  $scope.nbImgUtilise;
  $scope.loading = true;
  $scope.enCours = false;
  $scope.scoreImg = "Aucun score pour le moment";
  $scope.num = 0;
  $rootScope.answers = new Array();

  $scope.partie = Partie.get({
    id: $routeParams['id']
  });
  // console.log(partie);
  $scope.partie.$promise.then(function() {
    // console.log(partie.serie);
    $scope.serieJeu = $scope.partie.serie;
    $scope.serie = $scope.serieJeu;
    $scope.images = Photo.get({
      idPhoto: $scope.partie.serie.id
    });
    $scope.images.$promise.then(function() {
      var img;
      $scope.loading = false;
      $scope.nbImg = $scope.images.photos.length;
      $scope.i = 0;

      $scope.scoreTotal = 0;
      $rootScope.cliquable = false;
      $rootScope.mapMarkers = new Array();

      $scope.afficherImg = function(numImg) {
        $scope.urlPhoto = window.ws + '/photolocate/' + img.url;
        $scope.description = img.description;
      }

      var randomImage = function() {
        img = $scope.images.photos[Math.floor(Math.random() * $scope.images.photos.length)];
        $scope.alreadyUsed = false;
        if ($scope.tableauDistPhoto != null || ableauDistPhoto.length != 0) {
          angular.forEach($scope.tableauDistPhoto, function(value, key) {
            if (angular.equals(value.objImage, img)) {
              $scope.alreadyUsed = true;
              img = $scope.images.photos[Math.floor(Math.random() * $scope.images.photos.length)];
            } else {
              $scope.alreadyUsed = false;
            }
          });
          while ($scope.alreadyUsed == true) {
            img = $scope.images.photos[Math.floor(Math.random() * $scope.images.photos.length)];
            angular.forEach($scope.tableauDistPhoto, function(value, key) {
              if (angular.equals(value.objImage, img)) {
                $scope.alreadyUsed = true;
              } else {
                $scope.alreadyUsed = false;
              }
            });
          }
        }
      }

      var afficherImgSuivante = function() {
        $scope.num++;
        if ($scope.nbImg > 10) { //Si le nombre d'image dans la série est supérieur à 10
          $scope.nbImgUtilise = 10;
          if ($scope.num < 10) { // alors on fait un parcour jusqu'à 10 maximum
            $scope.i = 0;
            $scope.tempsAffichage = 10;
            $scope.afficherImg();
            $scope.temps();
            $rootScope.cliquable = true;
          } else {
            //FIN DE LA PARTIE !
            $scope.tableauDistPhoto = new Array();
            $scope.num = 0;
            $rootScope.answers = new Array();
            //Ici on envoie les scores
            putPartie($scope.partie, $scope.scoreTotal, $scope.nbImgUtilise);
          }
        } else { //Sinon
          $scope.nbImgUtilise = $scope.nbImg;
          if ($scope.num < $scope.nbImg) { //on fait un parcour jusqu'au maximum du nombre d'image
            $scope.i = 0;
            $scope.tempsAffichage = 10;
            $scope.afficherImg();
            $scope.temps();
            $rootScope.cliquable = true;
          } else {
            //FIN DE LA PARTIE !
            $scope.tableauDistPhoto = new Array();
            $scope.num = 0;
            $rootScope.answers = new Array();
            //Ici on envoie les scores
            putPartie($scope.partie, $scope.scoreTotal, $scope.nbImgUtilise);
          }
        }
      }

      var calculeDistance = function() {
        $scope.cooPhoto = getCooPhoto();
        if ($scope.cooPhoto !== null) {
          $scope.argument = $rootScope.mapMarkers[$rootScope.mapMarkers.length - 1];
          $scope.dist = Math.round($scope.argument.distanceTo($scope.cooPhoto));
          if ($scope.dist > 1) {
            $scope.distance = "Vous avez cliqué à " + $scope.dist + " mètres du lieu.";
          } else {
            $scope.distance = "Vous avez cliqué à " + $scope.dist + " mètre du lieu.";
          }
          $scope.tableauDistPhoto.push({
            objImage: img,
            distance: $scope.dist,
            temps: $scope.i,
            score: calculScore($scope.dist, $scope.i)
          });
          $scope.scoreImg = calculScore($scope.dist, $scope.i);
          $scope.scoreTotal += $scope.scoreImg;
        }

        $rootScope.answers = new Array();
        $rootScope.answers.push({
          lat: img.lat,
          lon: img.lon
        });
      }

      var getCooPhoto = function() {
        if ($scope.num < $scope.nbImg) {
          $scope.cooPhoto = {
            lat: img.lat,
            lng: img.lon
          };
          return $scope.cooPhoto;
        } else {
          return null;
        }
      }

      // Fonction timer
      var stop;
      $scope.temps = function() {
        if (angular.isDefined(stop)) return;
        $rootScope.cliquable = true;
        $scope.enCours = true;
        randomImage();
        stop = $interval(function() {
          if ($scope.i < 11) {
            $scope.i++;
            $scope.tempsAffichage = 11 - $scope.i;
            $scope.afficherImg();
          } else {
            $scope.stopDecompte();
            $timeout(function() {
              afficherImgSuivante();
            }, 1000);
          }
        }, 1000);
      };

      $scope.stopDecompte = function() {
        if (angular.isDefined(stop)) {
          $interval.cancel(stop);
          stop = undefined;
        }
      };

      $scope.$on('$imageSuite', function() {
        // Make sure that the interval is destroyed too
        $scope.stopDecompte();
      });

      $scope.$on('leafletDirectiveMap.click', function(event, args) {
        $scope.stopDecompte();
      });

      $rootScope.$watch('mapMarkers', function() {
        if ($rootScope.mapMarkers.length > 0) {
          calculeDistance();
          $rootScope.cliquable = false;
          $timeout(function() {
            $rootScope.mapMarkers = new Array();
            afficherImgSuivante();
          }, 1000);
        }
      }, true);
    });

    var calculScore = function(distance, temps) {
      var score_Img = 0;
      if (parseInt(distance) < parseInt($scope.serieJeu.distance)) {
        score_Img = 5;
      } else if (parseInt(distance) < parseInt($scope.serieJeu.distance) * 2) {
        score_Img = 3;
      } else if (parseInt(distance) < parseInt($scope.serieJeu.distance) * 3) {
        score_Img = 1;
      } else {
        score_Img = 0;
      }

      if (temps <= 2) {
        score_Img *= 4;
      } else if (temps <= 5) {
        score_Img *= 2
      } else if (temps > 10) {
        score_Img = 0;
      }

      return score_Img;
    }
    var putPartie = function(partie, newScore, nbPhotos) {
      var p = new Partie({
        id: partie.id,
        joueur: partie.joueur,
        nbPhotos: nbPhotos,
        score: newScore,
        serie: partie.serie,
        status: 'finished',
        token: partie.token
      });

      $http.put(window.ws + window.api + "/parties/" + partie.id, p, {
          headers: {
            'Authorization': 'token ' + partie.token
          }
        })
        .success(function(response) {
          $location.path('/parties/' + partie.id + '/scores');
          $scope.$destroy();
          $scope
        })
        .error(function() {});
    }
  });
  // }

  // $scope.new();

}]).directive('currentTime', ['$interval', 'dateFilter', function($interval, dateFilter) {
  // return the directive link function.
  return function(scope, element, attrs) {
    var format, //date format
      stopTime; //Pour pouvoir arreter la mise a jour du temps

    //Met à jour l'interface utilisateur
    function updateTime() {
      element.text(dateFilter(new Date(), format));
    }

    //Surveille l'expression et met à jour
    scope.$watch(attrs.currentTime, function(value) {
      format = value;
      updateTime();
    });

    stopTime = $interval(updateTime, 1000);

    element.on('$imageSuite', function() {
      $interval.cancel(stopTime);
    });
  }
}]);
