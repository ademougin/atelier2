package com.airhacks;

import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import org.boundary.PartiesRepresentation;
import org.boundary.PhotosRepresentation;
import org.boundary.SeriesRepresentation;
import org.boundary.TokenBoundary;
import org.boundary.UploadService;
import provider.AuthentificationFiltre;
import provider.Securise;
import provider.CORSFilter;
import provider.CORSResponseFilter;

/**
 * Configures a JAX-RS endpoint. Delete this class, if you are not exposing
 * JAX-RS resources in your application.
 *
 * @author airhacks.com
 */
@ApplicationPath("api")
public class JAXRSConfiguration extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> s = new HashSet<>();
        s.add(UploadService.class);
        s.add(PartiesRepresentation.class);
        s.add(PhotosRepresentation.class);
        s.add(SeriesRepresentation.class);
        s.add(TokenBoundary.class);
        s.add(Securise.class);
        s.add(AuthentificationFiltre.class);
        s.add(CORSFilter.class);
        s.add(CORSResponseFilter.class);        
        s.add(org.glassfish.jersey.filter.LoggingFilter.class);
        s.add(org.glassfish.jersey.media.multipart.MultiPartFeature.class);
        return s;
    }
    
}
