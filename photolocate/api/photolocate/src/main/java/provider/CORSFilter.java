package provider;

import java.io.IOException;
import java.util.logging.Logger;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

@Provider
@PreMatching
public class CORSFilter implements ContainerRequestFilter {

	private final static Logger log = Logger.getLogger(CORSFilter.class.getName());

	@Override
	public void filter(ContainerRequestContext requestCtx) throws IOException {
		log.info("Executing REST request filter");
		if (requestCtx.getRequest().getMethod().equals("OPTIONS")) {
			log.info("HTTP Method (OPTIONS) - Detected!");
			requestCtx.abortWith(Response.status(Response.Status.OK).build());
		}
	}
}
