package provider;

import java.io.IOException;
import javax.inject.Inject;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import org.boundary.PartiesResource;
import org.entity.Partie;

@Securise
@Provider
public class AuthentificationFiltre implements ContainerRequestFilter {

    @Inject
    PartiesResource ressource;
    
    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException{
        String authHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
        if(authHeader==null || !authHeader.startsWith("token ")){
            throw new NotAuthorizedException("Probleme d'authentification header");
        } 
        
        //on recupere l'id dans l'url
        String url = (requestContext.getUriInfo().getAbsolutePath()).toString();
        int id = Integer.parseInt(url.substring(url.indexOf("parties/")+8));

        //on extrait le token
        String token = authHeader.substring("token".length()).trim();

        //on verifie si le token correspond a celui de la bdd
        try{
            valideToken(token, id);
        }
        catch(Exception e){
            requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
        }
    }
    
    private void valideToken(String token, int id){
        Partie partie = ressource.findById(id);
        String tokenBDD = partie.getToken();
        
        //on authentifie grace à la BD
        if(!token.equals(tokenBDD)){
            throw new ForbiddenException("Token invalide");
        }
    }
}
