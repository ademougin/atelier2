package org.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "partie")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Partie.findAll", query = "SELECT p FROM Partie p"),
    @NamedQuery(name = "Partie.findById", query = "SELECT p FROM Partie p WHERE p.id = :id"),
    @NamedQuery(name = "Partie.findBestScores", query = "SELECT p FROM Partie p WHERE p.status='finished' ORDER BY p.score DESC")})
public class Partie implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Basic(optional = false)
    @NotNull
    @Column(name = "token")
    private String token;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "nb_photos")
    private int nbPhotos;

    @Column(name = "status")
    private String status;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "score")
    private int score;
    
    @JoinColumn(name = "id_serie", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Serie serie;
    
    @Column(name = "joueur")
    private String joueur;

    public Partie() {
    }

    public Partie(String token, int nbPhotos, int score, String status, Serie serie,String joueur) {
        this.token = token;
        this.nbPhotos = nbPhotos;
        this.score = score;
        this.status = status;
        this.serie = serie;
        this.joueur = joueur;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    public String getJoueur() {
        return joueur;
    }

    public void setJoueur(String joueur) {
        this.joueur = joueur;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getNbPhotos() {
        return nbPhotos;
    }

    public void setNbPhotos(int nbPhotos) {
        this.nbPhotos = nbPhotos;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public Serie getSerie() {
        return serie;
    }

    public void setSerie(Serie serie) {
        this.serie = serie;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Partie)) {
            return false;
        }
        Partie other = (Partie) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.entity.Partie[ id=" + id + " ]";
    }
    
}
