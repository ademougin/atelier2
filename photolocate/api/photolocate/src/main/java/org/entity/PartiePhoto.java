///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package org.entity;
//
//import java.io.Serializable;
//import javax.persistence.Basic;
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.JoinColumn;
//import javax.persistence.ManyToOne;
//import javax.persistence.NamedQueries;
//import javax.persistence.NamedQuery;
//import javax.persistence.Table;
//import javax.xml.bind.annotation.XmlRootElement;
//
///**
// *
// * @author Alban
// */
//@Entity
//@Table(name = "partie_photo")
//@XmlRootElement
//@NamedQueries({
//    @NamedQuery(name = "PartiePhoto.findAll", query = "SELECT p FROM PartiePhoto p"),
//    @NamedQuery(name = "PartiePhoto.findByIdPartiePhoto", query = "SELECT p FROM PartiePhoto p WHERE p.idPartiePhoto = :idPartiePhoto")})
//public class PartiePhoto implements Serializable {
//
//    private static final long serialVersionUID = 1L;
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Basic(optional = false)
//    @Column(name = "id_partie_photo")
//    private Integer idPartiePhoto;
//    @JoinColumn(name = "id_partie", referencedColumnName = "id_partie")
//    @ManyToOne(optional = false)
//    private Partie idPartie;
////    @JoinColumn(name = "id_photo", referencedColumnName = "id_photo")
////    @ManyToOne(optional = false)
//    private Photo idPhoto;
//
//    public PartiePhoto() {
//    }
//
//    public PartiePhoto(Integer idPartiePhoto) {
//        this.idPartiePhoto = idPartiePhoto;
//    }
//
//    public Integer getIdPartiePhoto() {
//        return idPartiePhoto;
//    }
//
//    public void setIdPartiePhoto(Integer idPartiePhoto) {
//        this.idPartiePhoto = idPartiePhoto;
//    }
//
//    public Partie getIdPartie() {
//        return idPartie;
//    }
//
//    public void setIdPartie(Partie idPartie) {
//        this.idPartie = idPartie;
//    }
//
//    public Photo getIdPhoto() {
//        return idPhoto;
//    }
//
//    public void setIdPhoto(Photo idPhoto) {
//        this.idPhoto = idPhoto;
//    }
//
//    @Override
//    public int hashCode() {
//        int hash = 0;
//        hash += (idPartiePhoto != null ? idPartiePhoto.hashCode() : 0);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object object) {
//        // TODO: Warning - this method won't work in the case the id fields are not set
//        if (!(object instanceof PartiePhoto)) {
//            return false;
//        }
//        PartiePhoto other = (PartiePhoto) object;
//        if ((this.idPartiePhoto == null && other.idPartiePhoto != null) || (this.idPartiePhoto != null && !this.idPartiePhoto.equals(other.idPartiePhoto))) {
//            return false;
//        }
//        return true;
//    }
//
//    @Override
//    public String toString() {
//        return "org.entity.PartiePhoto[ idPartiePhoto=" + idPartiePhoto + " ]";
//    }
//    
//}
