/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.boundary;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.CacheStoreMode;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.entity.Photo;
import org.entity.Serie;

@Stateless
public class SeriesResource {

    @PersistenceContext
    EntityManager em;

    public Serie findById(int id) { 
        Query q = this.em.createNamedQuery("Serie.findById", Serie.class).setParameter("id", id);
        q.setHint("javax.persistence.cache.storeMode", CacheStoreMode.REFRESH);
        return (Serie)q.getSingleResult();
    }

    public List<Serie> findAll() {
        Query q = this.em.createNamedQuery("Serie.findAll", Serie.class);
        q.setHint("javax.persistence.cache.storeMode", CacheStoreMode.REFRESH);
        return q.getResultList();
    }

    public Serie save(Serie ins) {
        return this.em.merge(ins);
    }
    
    public List<Photo> findPhotos(int id){
        Query q = this.em.createNamedQuery("Serie.findPhotos").setParameter("id",id);
        q.setHint("javax.persistence.cache.storeMode", CacheStoreMode.REFRESH);
        return q.getResultList();
    }        
}
