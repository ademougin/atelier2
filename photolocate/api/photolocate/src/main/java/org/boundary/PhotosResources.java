/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.boundary;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.CacheStoreMode;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.entity.Photo;

@Stateless
public class PhotosResources {
   
    @PersistenceContext
    EntityManager em;

    public List<Photo> findAll() {
        Query q = this.em.createNamedQuery("Photo.findAll", Photo.class);
        q.setHint("javax.persistence.cache.storeMode", CacheStoreMode.REFRESH);
        return q.getResultList();
    }
    
    public Photo findById(int id) {  
        Query q = this.em.createNamedQuery("Photo.findById", Photo.class).setParameter("id", id);
        q.setHint("javax.persistence.cache.storeMode", CacheStoreMode.REFRESH);
        return (Photo)q.getSingleResult();
    }

    public Photo save(Photo ins) {
        return this.em.merge(ins);
    }
    
    public List<Photo> findBySerie(int id) {    
        Query q = this.em.createNamedQuery("Photo.findBySerie", Photo.class).setParameter("id",id);
        q.setHint("javax.persistence.cache.storeMode", CacheStoreMode.REFRESH);
        return q.getResultList();
 }     
}
