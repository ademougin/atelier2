package org.boundary;

import java.net.URI;
import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import org.entity.Photo;
import org.entity.Serie;

@Path("series")
public class SeriesRepresentation {

    @Inject
    SeriesResource resource;

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("{id}")
    public Response find(@PathParam("id") int id) {
        Serie ins = this.resource.findById(id);
        if (ins != null) {
            return Response.ok(ins).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Serie> all() {
        return this.resource.findAll();
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Serie update(@PathParam("id") int id, Serie ins) {
        ins.setId(id);
        return this.resource.save(ins);
    }

    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes(MediaType.APPLICATION_JSON)
    public Response save(Serie ins, @Context UriInfo info) {
        Serie saved = this.resource.save(ins);
        Integer id = saved.getId();
        URI uri = info.getAbsolutePathBuilder().path("/" + id).build();
        return Response.created(uri).entity(saved).build();
    }

    
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("{id}/photos")
    public Response findWithPhoto(@PathParam("id") int id) {
        Serie ins = this.resource.findById(id);
        List<Photo> photos = this.resource.findPhotos(id);
        ins.setPhotos(photos);
        
        if (ins != null) {
            return Response.ok(ins).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

}