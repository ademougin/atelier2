package org.boundary;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.CacheStoreMode;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.entity.Partie;

@Stateless
public class PartiesResource {
    @PersistenceContext
    EntityManager em;

    public Partie findById(int id) {
        Query q = this.em.createNamedQuery("Partie.findById", Partie.class).setParameter("id", id);
        q.setHint("javax.persistence.cache.storeMode", CacheStoreMode.REFRESH);
        return (Partie)q.getSingleResult();
    }

    public List<Partie> findAll() {
        Query q = this.em.createNamedQuery("Partie.findAll", Partie.class);
        q.setHint("javax.persistence.cache.storeMode", CacheStoreMode.REFRESH);
        return q.getResultList();
    }

    public Partie save(Partie ins) {
        return this.em.merge(ins);
    }
    
    public List<Partie> findBestScores() {
        Query q = this.em.createNamedQuery("Partie.findBestScores", Partie.class).setMaxResults(3);
        q.setHint("javax.persistence.cache.storeMode", CacheStoreMode.REFRESH);
        return q.getResultList();    
    } 
    
}
