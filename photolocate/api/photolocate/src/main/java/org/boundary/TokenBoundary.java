package org.boundary;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;
import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("token")
public class TokenBoundary {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response retournResponseToken(){
        try{
            String token = generationToken();
            JsonObject jToken = Json.createObjectBuilder().add("token",token).build();
            
            return Response.ok(jToken).build();
            
        }catch (Exception e){
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }    
    }
    
    public String retourneStringToken(){
        try{
            String token = generationToken();
            
            return token;
            
        }catch (Exception e){
            return "-1";
        }    
    }
    
    private String generationToken() throws NoSuchAlgorithmException {
        UUID random = UUID.randomUUID();
        String token = String.valueOf(random);
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(token.getBytes());
        byte byteData[]=md.digest();
        //convertit en String
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < byteData.length ; i++){
            sb.append(Integer.toString(byteData[i] & 0xff + 0x100, 16).substring(1));
        }
        return sb.toString();

    }
}

