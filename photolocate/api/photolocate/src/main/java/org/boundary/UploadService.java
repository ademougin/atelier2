package org.boundary;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

@Path("img")
public class UploadService {
 @POST
 @Path("upload")
 @Consumes({MediaType.MULTIPART_FORM_DATA})
 public Response uploadFichier(
                               @FormDataParam("file") InputStream uploadInputStream,
                               @FormDataParam("file") FormDataContentDisposition fileDetail) {


    //attention doit être accèssible par le serveur d'application (de préférence mettre dans le domaine
    String uploadFileLocation = "images/" + fileDetail.getFileName();
    writeToFile(uploadInputStream, "/Users/Alban/Google Drive/Cours/##ServeurLocal##/ATELIER2/photolocate/api/photolocate/src/main/webapp/"+uploadFileLocation );
    String ouput = uploadFileLocation;

    return Response.status(200).entity(ouput).build();
}

private void writeToFile(InputStream uploadInputStream,String uploadFileLocation) {
    try {
        int read = 0;
        byte[] bytes = new byte[1024];
        OutputStream out = new FileOutputStream(new File(uploadFileLocation));

        while((read = uploadInputStream.read(bytes)) != -1) {
            out.write(bytes,0,read);
        }
        out.flush();
        out.close();
    } catch (IOException ioe) {
        ioe.printStackTrace();
    }
}
}
