package org.boundary;

import java.net.URI;
import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import org.entity.Photo;

@Path("photos")
public class PhotosRepresentation {
    
    @Inject
    PhotosResources ressource;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Photo> getAll() {
        return ressource.findAll();
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("{id}")
    public Response find(@PathParam("id") int id, @Context UriInfo uriInfo) {
        Photo ins = this.ressource.findById(id);
        if (ins != null) {
            return Response.ok(ins).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }
    
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes(MediaType.APPLICATION_JSON) 
    public Response save(Photo ins, @Context UriInfo info) {
        Photo saved = this.ressource.save(ins);
        Integer id = saved.getId();
        URI uri = info.getAbsolutePathBuilder().path("/" + id).build();
        return Response.created(uri).entity(saved).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("series/{id}")
    public List<Photo> findBySerie(@PathParam("id") int id) {
        return this.ressource.findBySerie(id);
    }


}