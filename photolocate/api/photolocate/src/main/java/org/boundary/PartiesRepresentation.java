package org.boundary;

import java.net.URI;
import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import org.entity.Partie;
import provider.Securise;

@Path("parties")
public class PartiesRepresentation {
    
    @Inject
    PartiesResource ressource;

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("{id}")
    public Response find(@PathParam("id") int id) {
        Partie ins = this.ressource.findById(id);
        if (ins != null) {
            return Response.ok(ins).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Partie> all() {
        return this.ressource.findAll();
    }

    @PUT
    @Securise
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Partie update(@PathParam("id") int id, Partie ins) {
        ins.setId(id);
        return this.ressource.save(ins);
    }
    
    
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes(MediaType.APPLICATION_JSON)
    public Response save(Partie ins, @Context UriInfo info) {     
        //génération et sauvegarde du token
        TokenBoundary tokenBoundary = new TokenBoundary();
        ins.setToken(tokenBoundary.retourneStringToken());
        
        //attribution du status créé par défaut
        ins.setStatus("created");
        
        //sauvegarde en base
        Partie saved = this.ressource.save(ins);

        Integer id = saved.getId();
        URI uri = info.getAbsolutePathBuilder().path("/" + id).build();
        
        return Response.created(uri).entity(saved).build();
    }
    
    @GET
    @Path("scores")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Partie> bestScores() {
        return this.ressource.findBestScores();
    }
    
    
}