# recuperer la liste des series
GET

~~~~
/series
"series" : {
	"id" : ,
	"ville" : ,
	"lat" : ,
	"long" : ,
	"zoom" : ,
	"distance" : 
	}
~~~~


# creer une partie
POST

~~~~
/parties
"parties" : {
	"joueur" : ,
	"serie" :
	}
~~~~

201 : created

~~~~
	"id" : ,
	"token" : ,
	"score" : 0,
	"joueur" : ,
	"serie" : ,
	"status" "created"
~~~~

___

# recuperer la liste des photos de la serie
GET

~~~~
/series/:id/photos
"series" : {
	"id" : ,
	"ville" : ,
	"lat" : ,
	"long" : ,
	"zoom" : ,
	"distance" : ,
	"photos" : {
		"id" : ,
		"description" : ,
		"url" : ,
		"lat" : ,
		"lon" :
		}
	}
~~~~


# mettre à jour le status de la partie a en terminée une fois la partie finie
PUT [token]  

~~~~
/parties/:id
	"token" : ,
	"score" : ,
	"joueur" : ,
	"serie" : ,
	"status" "finished"
~~~~


# ADMIN : ajout de photo a une serie
POST

~~~~
/img/upload
{"file" : }
{
"url" : ""
}
~~~~

POST

~~~~
/photos
"categorie" : {
	"description" : ,
	"url" : ,
	"lat" : ,
	"lon" :
	}
~~~~
___
___

# HS recuperer 1 photo
GET

~~~~
/photos/:id
"categorie" : {
	"id" : ,
	"description" : ,
	"url" : ,
	"lat" : ,
	"lon" :
	}
~~~~

___

# Recuperer les photos pour une serie
GET

~~~~
/photos/series/{id}
"series" : {
	"id" : ,
	"ville" : ,
	"lat" : ,
	"long" : ,
	"zoom" : ,
	"distance" : ,
	"photos" : {
		"id" : ,
		"description" : ,
		"url" : ,
		"lat" : ,
		"lon" :
		}
	}
~~~~

200 : OK

404 : ID not found

400 : uri invalide
___


# Recuperer les meilleurs scores triés 
GET

~~~~
/parties/scores
~~~~

___


# Recuperer une serie sans ses photos
GET

~~~~
/series/:id
~~~~