# ************************************************************
# Sequel Pro SQL dump
# Version 4499
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Hôte: localhost (MySQL 5.5.42)
# Base de données: ATELIER
# Temps de génération: 2016-02-06 09:53:55 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

DROP DATABASE IF EXISTS `ATELIER`;

CREATE DATABASE ATELIER CHARACTER SET 'utf8';

USE ATELIER;


# Affichage de la table partie
# ------------------------------------------------------------

DROP TABLE IF EXISTS `partie`;

CREATE TABLE `partie` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `token` varchar(64) DEFAULT '',
  `nb_photos` int(3) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `score` int(10) DEFAULT NULL,
  `id_serie` int(10) DEFAULT NULL,
  `joueur` varchar(20) DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `fk_serie_partie_id_partie_idx` (`id_serie`),
  CONSTRAINT `partie_ibfk_1` FOREIGN KEY (`id_serie`) REFERENCES `serie` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `partie` WRITE;
/*!40000 ALTER TABLE `partie` DISABLE KEYS */;

INSERT INTO `partie` (`id`, `token`, `nb_photos`, `status`, `score`, `id_serie`, `joueur`)
VALUES
	(11,'0cabedb9fa8f69ce9f85c1bcdfffd5bf4ca14689200ad9f9',10,'finished',36,2,'Victorien'),
	(45,'8cd8e36deb4fc808adebb2be319d049593d37fb9bf1e8f09bf',5,'finished',36,4,'Clara'),
	(49,'c5b21dbed939038eddc775dbcb18fbdecf0cab4fcfdea826',5,'finished',56,4,'Alban'),
	(52,'906fa009f899dafcec2893c92e91181fc8bdb8d920fb',5,'finished',60,4,'Fabien'),
	(54,'f09bd5cdb40e7b54bfa7aea9b468fb4f1e8bfeef0e5e93f9a0f5',5,'finished',22,4,'F'),
	(57,'d0d5fdaad3fc0935e5fbda80a4e3d927c93c95e9b5c0f',5,'finished',34,4,'F'),
	(58,'edaeb4f12efe87bea2f65abbcee31bb16eaf0a6c8ec3f65',0,'created',0,2,'F');

/*!40000 ALTER TABLE `partie` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table partie_photo
# ------------------------------------------------------------

DROP TABLE IF EXISTS `partie_photo`;

CREATE TABLE `partie_photo` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_partie` int(10) DEFAULT NULL,
  `id_photo` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_photo_id_photo_idx` (`id_photo`),
  KEY `fk_partie_id_partie_idx` (`id_partie`),
  CONSTRAINT `partie_photo_ibfk_1` FOREIGN KEY (`id_partie`) REFERENCES `partie` (`id`),
  CONSTRAINT `partie_photo_ibfk_2` FOREIGN KEY (`id_photo`) REFERENCES `photo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Affichage de la table photo
# ------------------------------------------------------------

DROP TABLE IF EXISTS `photo`;

CREATE TABLE `photo` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) DEFAULT '',
  `lat` varchar(30) DEFAULT '',
  `lon` varchar(30) DEFAULT '',
  `description` varchar(100) DEFAULT NULL,
  `id_serie` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_seire_photo_id_serie_idx` (`id_serie`),
  CONSTRAINT `photo_ibfk_1` FOREIGN KEY (`id_serie`) REFERENCES `serie` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `photo` WRITE;
/*!40000 ALTER TABLE `photo` DISABLE KEYS */;

INSERT INTO `photo` (`id`, `url`, `lat`, `lon`, `description`, `id_serie`)
VALUES
	(1,'images/arctriomphe.jpg','48.873779','2.295016','Arc de Triomphe',2),
	(2,'images/sacrecoeur.jpg','48.886653','2.342582','Sacré Coeur',2),
	(3,'images/toureiffel.jpg','48.858261','2.294507','Tour Eiffel',2),
	(4,'images/14606.jpg','48.8609869759575','2.335864501073956','Pyramide du Louvre',2),
	(27,'images/universite-la-sorbonne-paris.jpg','48.8487219887559','2.343651205301285','La Sorbonne',2),
	(28,'images/pont-neuf.jpg','48.85774685655656','2.3419252038002014','Pont Neuf',2),
	(31,'images/place-vendome.jpg','48.867441884424295','2.3293818533420563','Place Vendôme',2),
	(32,'images/opera-garnier.jpg','48.87203255435402','2.3318226635456085','Opera Garnier',2),
	(33,'images/Notre_Dame_de_Paris_DSC_0846w.jpg','48.85284491981177','2.3501475155353546','Notre Dame de Paris',2),
	(34,'images/musee-orsay.jpg','48.859935708245494','2.3266084492206573','Musee d\' Orsay',2),
	(35,'images/moulin-rouge.jpg','48.883921877709376','2.332638055086136','Moulin Rouge',2),
	(36,'images/elysee.jpg','48.87043415981371','2.315981537103653','Palais de l\'Elysée',2),
	(37,'images/cite-des-sciences.jpg','48.895691992946496','2.388020306825638','Cité des Sciences et de l\'Industrie',2),
	(39,'images/centre-pompidou.jpg','48.898284079314756','2.2808070480823517','Centre Pompidou',2),
	(41,'images/statuedelaliberte.jpg','40.68915131772666','-74.04452428221703','Statue de la Liberté',4),
	(42,'images/Lower_Central_Park_Shot_5.JPG','40.77947717196697','-73.96661281585693','Central Park',4),
	(43,'images/Empire_State_Building_by_David_Shankbone_crop.jpg','40.74839819267458','-73.98563906550407','Empire State Building',4),
	(44,'images/Aerial_photo_of_WTC_groundzero.jpg','40.71150646585807','-74.01140302419662','Site du World Trade Center',4),
	(45,'images/1_times_square_night_2013.jpg','40.75899624215624','-73.9850473869592','Time Square',4),
	(48,'images/nancy4.jpg','48.689612075961534','6.174622178077698','Gare',8),
	(49,'images/dut.jpg','48.682832107321666','6.1610837280750275','IUT Charlemagne',8),
	(50,'images/0dd974055d.jpg','48.69353563413857','6.183355450630188','Place Stanislas',8);

/*!40000 ALTER TABLE `photo` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table serie
# ------------------------------------------------------------

DROP TABLE IF EXISTS `serie`;

CREATE TABLE `serie` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `ville` varchar(50) DEFAULT '',
  `distance` int(3) DEFAULT NULL,
  `lat` varchar(30) DEFAULT '',
  `lon` varchar(30) DEFAULT '',
  `zoom` int(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `serie` WRITE;
/*!40000 ALTER TABLE `serie` DISABLE KEYS */;

INSERT INTO `serie` (`id`, `ville`, `distance`, `lat`, `lon`, `zoom`)
VALUES
	(2,'Paris',150,'48.862682060035624','2.3349380493164062',12),
	(4,'New York',300,'40.730608477796636','-73.99137496948242',13),
	(8,'Nancy',80,'48.692054','6.184416999999939',14);

/*!40000 ALTER TABLE `serie` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
